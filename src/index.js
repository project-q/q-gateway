require('dotenv').config()

const { ApolloServer } = require('apollo-server')
const { ApolloGateway, RemoteGraphQLDataSource } = require('@apollo/gateway')
const http = require('http')
const faye = require('faye')
const config = require('../config')
const fetch = require('node-fetch')
const jwt = require('jsonwebtoken')

const fayeServer = http.createServer()
const bayeux = new faye.NodeAdapter({ mount: '/' })

bayeux.attach(fayeServer)
fayeServer.listen(8005)

const serviceList = [
  { name: 'products', url: process.env.SERVICE_PRODUCT_URL || 'http://localhost:3000/graphql' },
  { name: 'product_template', url: process.env.SERVICE_PRODUCTTEMPLATE_URL || 'http://localhost:3001/graphql' },
  { name: 'shopping_cart', url: process.env.SERVICE_SHOPPINGCART_URL || 'http://localhost:3002/graphql' },
  { name: 'orders', url: process.env.SERVICE_ORDER_URL || 'http://localhost:3003/graphql' },
  { name: 'account', url: process.env.SERVICE_ACCOUNT_URL || 'http://localhost:3004/graphql' },
  { name: 'auth', url: process.env.SERVICE_AUTH_URL || 'http://localhost:3005/graphql' },
  { name: 'media', url: process.env.SERVICE_MEDIA_URL || 'http://localhost:3006/graphql' },
  { name: 'image_recognition', url: process.env.SERVICE_IMAGERECOGNITION_URL || 'http://localhost:3007/graphql' },
];

const onlineServices = [];

const iterateList = new Promise((resolve, reject) => {
  let counter = 0
  serviceList.forEach(async (element, index) => {
    await fetch(element.url).then(function (response) {
      counter++
      onlineServices.push(element);
    })
      .catch(function (err) {
        counter++
        console.log(err.message)
      })
    if (counter == serviceList.length) {
      resolve(onlineServices)
    }
  });
})

iterateList.then(async (list) => {
  const gateway = new ApolloGateway({
    serviceList: list,
    buildService({ name, url }) {
      return new RemoteGraphQLDataSource({
        url,
        willSendRequest({ request, context }) {
          request.http.headers.set('user', JSON.stringify(getUser(context.req.headers.authorization)));
        },
      });
    },
  });
  const { schema, executor } = await gateway.load()
  const server = new ApolloServer({
    schema,
    executor,
    context: ({ req }) => ({
      req,
    }),
    playground: {
      endpoint: config.app.playground,
    },
    subscriptions: config.app.subscriptions,
  })

  function getUser(token) {
    try {
      if (token) {
        return jwt.verify(token, process.env.AUTH_KEY || 'jwtQ')
      }
      return null
    } catch (err) {
      return null
    }
  }

  server.listen({ port: process.env.GATEWAY_PORT || config.app.port, endpoint: config.app.endpoint }).then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`)
  })
})
