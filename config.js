module.exports = {
  app: {
    root: 'http://localhost',
    port: 4000,
    playground: '/',
    voyager: '/voyager',
    endpoint: '/graphql',
    subscriptions: '/subscriptions',
  }
}
